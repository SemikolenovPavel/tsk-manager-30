package ru.t1.semikolenov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.api.service.IAuthService;
import ru.t1.semikolenov.tm.api.service.IUserService;
import ru.t1.semikolenov.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    public IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    @NotNull
    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

}
