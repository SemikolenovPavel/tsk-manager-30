package ru.t1.semikolenov.tm.exception.field;

public final class EmptyLoginException extends AbstractFieldException {

    public EmptyLoginException() {
        super("Error! Login is empty...");
    }

}
