package ru.t1.semikolenov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.api.service.IProjectService;
import ru.t1.semikolenov.tm.api.service.IProjectTaskService;
import ru.t1.semikolenov.tm.command.AbstractCommand;
import ru.t1.semikolenov.tm.enumerated.Role;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    public IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    @NotNull
    public IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
