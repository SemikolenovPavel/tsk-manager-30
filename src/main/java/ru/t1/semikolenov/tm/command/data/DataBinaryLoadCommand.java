package ru.t1.semikolenov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.dto.Domain;
import ru.t1.semikolenov.tm.enumerated.Role;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public final class DataBinaryLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-bin";

    @NotNull
    public static final String DESCRIPTION = "Load data from binary file.";

    @Nullable
    public static final String ARGUMENT = null;

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @Nullable
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD BINARY]");
        @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        fileInputStream.close();
        setDomain(domain);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
